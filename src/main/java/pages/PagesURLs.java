package pages;

interface PagesURLs {

  static String base = "https://jira.hillel.it";
  static String loginPage = base + "/login.jsp";

}
